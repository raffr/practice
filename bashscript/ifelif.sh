#!/bin/bash

echo "input: "
read input

number=20

if [ $input -eq  $number ]
then
	echo "input is equal to $number"
else
	echo "input is not equal to $number"
fi


if (( $input == $number ))
then
	echo "$input is equal to $number"
elif (( $input < $number ))
then
	echo "$input < $number"
elif (( $input > $number ))
then
	echo "$input > $number"
else
	echo "wrong"
fi


if (( $input > 20 )) && (( $input < 30 ))
then
	echo "$input is more than 20 and $input is less than 30"
else 
	echo "false"
fi
