#!/bin/bash

# while loop
limit=10
number=1

while (( $number <= $limit  )) 
do
	echo "hello $number"
	number=$(( number+1 ))
done

# until loop
number=1

until (( $number < $limit ))
do
	echo "helo until loop $number"
	number=$(( number+1 ))
done

# for loop
for i in {0..20..5} #{start..end..increment}
do
	echo "forloop $i"
done

for ((i=0; i<=10; i=i+1)) #{start, end, increment}
do
	echo "for loop $i"
done

# break and continue statement
number=1
while true
do
	echo "$number"
	number=$(( number+1 ))
	if (( $number == 7 ))
	then 
		continue
	fi
	if (( $number == 10 ))
	then
		break
	fi
done
