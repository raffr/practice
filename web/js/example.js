//string
document.writeln("<h3>string</h3>") // writeln otomatis space
document.writeln("hello world")
document.writeln("</br>")
document.writeln("<textarea rows=10, cols=50>")
document.writeln("ini adalah\n->string")
document.writeln(10 + " adalah\n->integer")
document.write("ini adalah ")// write tidak otomatis space
document.write("string\n")// write tidak otomatis space
document.writeln("</textarea>")
document.writeln("</br><p>hello " + "world</p>")
document.writeln("<p>hello</p>" + "<u>world</u>")

//tipe data number
document.writeln("<h3>tipe data number</h3>")
document.writeln( 100 ) //decimal
document.writeln("<br/>")
document.writeln( 9.057 ) //float
document.writeln("<br/>")
document.writeln( 0b1101010010 ) //binary
document.writeln("<br/>")

//tipe data boolean
document.writeln("<h3>tipe data boolean</h3>")
document.writeln( true )
document.writeln("</br>")
document.writeln( false )

/* Variable */
//variable var
document.writeln("<h3>variable var</h3>")
var firstname = "Hello"
var lastname = "World"
var fullname = firstname + " " + lastname
document.writeln(fullname)
//variable let
document.writeln("<h3>variable let</h3>")
let firstname_let = "hello"
let lastname_let = "world"
let fullname_let = firstname_let + " " + lastname_let
document.writeln(fullname)
//variable const
document.writeln("<h3>variable constant</h3>")
const firstname_const = "Hello"
const lastname_const = "World"
const fullname_const = firstname_const + " " + lastname_const
document.writeln(fullname_const)

/* operator aritmatika */
document.writeln("<h3>operator aritmatika</h3>")
let angkaSatu = 100
let angkaDua = 150
let angkaTiga = 200
let Total
let total_tambah = angkaSatu + angkaDua
let total_kurang = angkaSatu - angkaDua
let total_kali = angkaSatu * angkaDua
let total_bagi = angkaSatu / angkaDua
let total_modulus = angkaSatu % angkaDua
document.writeln("angka " + angkaSatu + " + " + "angka " + angkaDua + " = " + total_tambah)
document.writeln("</br>")
document.writeln("angka " + angkaSatu + " - " + "angka " + angkaDua + " = " + total_kurang)
document.writeln("</br>")
document.writeln("angka " + angkaSatu + " * " + "angka " + angkaDua + " = " + total_kali)
document.writeln("</br>")
document.writeln("angka " + angkaSatu + " / " + "angka " + angkaDua + " = " + total_bagi)
document.writeln("</br>")
document.writeln("angka " + angkaSatu + " % " + "angka " + angkaDua + " = " + total_bagi)
document.writeln("</br>")
Total = angkaSatu * angkaDua / angkaTiga
document.writeln(Total,"</br>")
Total = angkaSatu / angkaDua % angkaTiga
document.writeln(Total,"</br>")
Total  = Total*100
document.writeln(Total,"</br>")

/* operator perbandingan javascript

> lebih dari
< kurang dari
>= lebih dari sama dengan
<= kurang dari sama dengan
== sama dengan
!= tidak sama dengan
=== sama dengan sama tipe
!== tdak sama dengan tidak sama tipe

 */

