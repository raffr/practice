<?php

echo<<<multiline

ini adalah contoh multi line php
multi line menggunakan heredoc
bisa enter tanpa slash n
dan bisa 		tab
tanpa slash t
lebih mudah

multiline;

echo<<<multiline

ini adalah contoh multiline dengan nowdoc
bisa enter dengan mudah
dan tab dengan mudah

multiline;


$angka = 1000;
$angka2 = 700;
$angka3 = 0.5;
$hasil = $angka - $angka2 * $angka3;
$kalimat = "contoh menggunakan variable di php";

echo "\nmemanggil variable di php:";
echo $angka;
echo "\nmemanggil variable di php:";
echo $angka2;
echo "\nmemanggil variable di php:";
echo $hasil;
echo "\nmemanggil variable di php:";
echo $kalimat;
echo "\n";

/*
variable di php bersifate mutable, dapat diubah ubah
untuk menggunakan immutable variable dapat meggunakan 
define("namavariable",value)
*/


define("CONTOH","hello");
define("ANGKA",100);
echo "immutable variable di php output:";
echo CONTOH; echo "\n";
echo "immutable variable php output kedua:";
echo ANGKA; echo"\n";

// constant
define("VARIABLENAME_INT",100);
define("VARIABLENAME_STR","hello world");
echo VARIABLENAME_INT;
echo "\n";
echo VARIABLENAME_STR;
echo "\n";

// null
echo "null\n";
$nama = "ini adalah variable string";
$alamat = "jupiter";
$nama = null;
echo $nama,"\n";
echo "\n";
echo isset($alamat);
echo "\n";
echo is_null($nama);
echo "\n";
echo isset($nama);
echo "\n";
echo is_null($alamat);
echo "\n";

// menghapus variable
$benda = "meja";
unset($benda);
echo "\n";
var_dump(isset($benda));
echo "\n";

// array
$list_pertama = [2,"hello","world","!"];
var_dump($list_pertama);
echo "\n";
echo $list_pertama[1];
echo "\n";
$list_kedua = array(
    "nama" => "contoh nama",
    "umur" => 30,
    "lang" => ["python","c","assembly"]
);

var_dump($list_kedua);
echo "\n";
echo $list_kedua["lang"][2];
echo "\n";

// operator aritmatika
$angka_satu = 100;
$angka_dua = 50;
$hasil = $angka_satu + $angka_dua;
echo $hasil;
echo "\n";
$hasil = $angka_satu - $angka_dua;
echo $hasil;
echo "\n";
$hasil = $angka_satu * $angka_dua;
echo $hasil;
echo "\n";
$hasil = $angka_satu / $angka_dua;
echo $hasil;
echo "\n";
$hasil = $angka_satu % $angka_dua;
echo $hasil;
echo "\n";

// operator penugasan
$angka_satu = 10;
$angka_dua = 21;
$angka_satu += $angka_dua;
echo $angka_satu;
echo "\n";
$angka_satu -= $angka_dua;
echo $angka_satu;
echo "\n";
$angka_satu /= $angka_dua;
echo $angka_satu;
echo "\n";
$angka_satu *= $angka_dua;
echo $angka_satu;
echo "\n";
$angka_satu %= $angka_dua;
echo $angka_satu;
echo "\n";

// operator perbandingan
$intA = 100;
$stringA = "100";
$intB = 200;
$stringB = "200";

var_dump($intA == $stringA);
var_dump($intA == $stringA);
var_dump($intA != $stringA);
var_dump($intA !== $stringA);
var_dump($intA < $intB);
var_dump($intA <= $intB);
var_dump($intA > $intB);
var_dump($intA >= $intB);
var_dump($intB == $stringB);
var_dump($intB === $stringB);
var_dump($intB == $stringB);
var_dump($intB === $stringB);

// operator logika
echo PHP_EOL . "operator logika" . PHP_EOL;
$hasil = true and true;
var_dump($hasil);
$hasil = true && true;
var_dump($hasil);
$hasil = false or true;
var_dump($hasil);
$hasil = false || true;
var_dump($hasil);

// ternary operator
$jawaban = true;
$soal = $jawaban ? "benar":"salah";
echo $soal . PHP_EOL;
echo PHP_EOL;

// percabangan
$jawaban = "dua";
if($jawaban == "satu"){
    echo "jawaban " . $jawaban . " benar" . PHP_EOL;
} else if ($jawaban == "dua" || $jawaban == "nol"){
    echo "jawaban " . $jawaban . " sedikit benar" . PHP_EOL;
} else {
    echo "salah kaprah" . PHP_EOL;
}

echo PHP_EOL;

switch ($jawaban){
    case "satu":
        echo "jawaban " . $jawaban . " benar" . PHP_EOL;
        break;
    case "dua":
    case "nol":
        echo "jawaban " . $jawaban . " sedikit benar" . PHP_EOL;
        break;
    default:
        echo "salah kaprah", PHP_EOL;
        break;
}

echo PHP_EOL;

$username = "hello";
$password = "world";

if($username == "hello"){
    if($password == "world"){
        echo "welcome!" . PHP_EOL;
    } else {
        echo "password salah" . PHP_EOL;
    }
} else {
    echo "username salah" . PHP_EOL;
}

// perulangan for loop, while, do while, for each
