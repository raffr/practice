import paramiko
from getpass import getpass

print(" === ssh to server === ")
user = (input("username (default vm1): ") or "vm1")
host = (input("address (default 192.168.122.129): ") or "192.168.122.129")
password = getpass()

client = paramiko.client.SSHClient()

client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

client.connect(hostname=host, username=user, password=password)

stdin, stdout, stderr = client.exec_command(f"""
    lsblk
    free -m
""")

print(stdout.read().decode())

client.close()