# Access modifier public
# Variabel atau atribut yang memiliki hak akses publik bisa diakses dari
# mana saja baik dari luar kelas mau pun dari dalam kelas.
# contoh : self.nama self.mangsa
#
# Access modifier protected
# Variabel atau atribut yang memiliki hak akses protected hanya bisa diakses
# secara terbatas oleh dirinya sendiri (yaitu di dalam internal kelas),
# dan juga dari kelas turunannya.
# contoh : self._nama, self._mangsa
#
# Private Access Modifier
# Modifier selanjutnya adalah private. Setiap variabel di dalam suatu
# kelas yang memiliki hak akses private maka ia hanya
# bisa diakses di dalam kelas tersebut.
# Tidak bisa diakses dari luar bahkan dari kelas yang mewarisinya.
# contoh : self.__nama , self.__mangsa


class Hewan:
    def __init__(self,nama,mangsa,kecepatan):
        self.nama = nama
        self._mangsa = mangsa
        self.__kecepatan = kecepatan
    def kecepatan(self):
        print(self.__kecepatan)
class mamalia(Hewan):
    def output(self):
        print(self.nama, self._mangsa)

kucing = mamalia("kucing","ikan","100km/hr")
kucing.kecepatan()
print(kucing.output())
