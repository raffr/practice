class Kendaraan:
    def __init__(self,nama,roda,bahan_bakar,jalan):
        self.nama = nama
        self.roda = roda
        self.bahan_bakar = bahan_bakar
        self.jalan = jalan
    def output(self):
        print("Nama Kendaraan\t\t:",self.nama)
        print("Jumlah Roda\t\t:",self.roda)
        print("Bahan Bakar\t\t:",self.bahan_bakar)
        print("Darat/Udara\t\t:",self.jalan)

class Tank(Kendaraan):
    def __init__(self,nama):
        self.roda = 2
        self.bahan_bakar = "bensin"
        self.jalan = "darat"
        Kendaraan.__init__(self,nama,self.roda,self.bahan_bakar,self.jalan)

class Sepeda(Kendaraan):
    def __init__(self,nama):
        self.roda = 2
        self.bahan_bakar = "manual"
        self.jalan = "darat"
        super().__init__(nama,self.roda,self.bahan_bakar,self.jalan)

tank_baja = Tank("tank baja")
tank_baja.output()

print()

sepeda_gunung = Sepeda("sepeda gunung")
sepeda_gunung.output()
