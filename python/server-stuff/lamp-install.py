import os

print("""
##########################################################
# Install Apache2 Mariadb-server PHP composer
######################
        """)

# Install requiered package
package_list = "libapache2-mod-php php php-common php-xml php-gd php-opcache php-mbstring php-tokenizer php-json php-bcmath php-zip unzip curl mariadb-server"

print("List package will be installed: ")
print(package_list,"\n")

loop = True
while True:
    add_package = input("==> add another package?(y/n) Default no\n>")
    if add_package.lower() == "y" or add_package.lower() == "yes":
        input_package = input("\n===> add package:\n")
        os.system(f"apt update && apt install {package_list} {input_package} -y")
        break
    elif add_package.lower() == "n" or add_package.lower() == "no" or add_package == "":
        os.system(f"apt update && apt install {package_list} -y")
        break
    else:
        print("wrong input!")


# add apache virtualhost and enable it
virtualhost_string = """
<Virtualhost *:80>
    DocumentRoot /var/www/web/
</Virtualhost>"""


os.system(f"echo \"{virtualhost_string}\" > /etc/apache2/sites-available/installerconfig.conf && cd /etc/apache2/sites-available/ && a2ensite installerconfig.conf && a2enmod php7.4 && a2dissite 000-default.conf && a2dismod mpm_event && a2enmod mpm_prefork")

#add php file example
php_string = """
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <div style='height=43;background-color:blueviolet;border-radius:5px'>
    <h1 style='color:white; text-align:center;'>Installer Work!</h1>
    </div>
    <p style='color:blueviolet; text-align:center;'>
    <?php 
        echo 'PHP and Apache work!';
    ?>
    </p>

</body>
</html>
"""
os.system("mkdir -p /var/www/web")
os.system(f"echo \"{php_string}\" > /var/www/web/index.php")

# restart and enable apache and mariadb services
os.system("service apache2 restart")
os.system("service mariadb restart")
os.system("service apache2 reload")
os.system("service mariadb reload")

print("""
#######################################################
# DONE!
############################
        """)
