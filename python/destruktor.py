class Siswa:
    def __init__(self,nama):
        self.nama = nama
        print(f"nama '{self.nama}' dibuat")
    def __del__(self):
        print(f"{self.nama} dihapus")

contoh = Siswa("contoh")
print(contoh.nama)
