class Hewan:
    def __init__(self,nama,kaki):
        self.nama = nama
        self.kaki = kaki
    def output(self):
        print(f"Nama Hewan\t: {self.nama}")
        print(f"Jumlah Kaki\t: {self.kaki}")

class Mamalia(Hewan):
    def __init__(self,nama,kaki):
        Hewan.__init__(self,nama,kaki)
        self.jenis = "Mamalia"
        print("jenis binatang", self.jenis)

class Unggas(Hewan):
    def __init__(self,name,legs):
        Hewan.__init__(self,name,legs)
        self.jenis = "Unggas"
        print("Jenis binatang",self.jenis)

kucing = Mamalia("kucing",4)
kucing.output()
print()
elang = Unggas("elang",2)
elang.output()
