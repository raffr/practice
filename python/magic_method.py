# Method __str__() akan dipanggil ketika sebuah instan dari kelas yang
# bersangkutan dikonversi menjadi string atau ketika dijadikan parameter
# untuk fungsi print().
class Penjumlahan:
    def __init__(self,angka1,angka2):
        self.angka1 = angka1
        self.angka2 = angka2
    def __str__(self):
        self.hasil = self.angka1 + self.angka2
        return f"hasil {self.angka1}+{self.angka2} adalah {self.hasil}"

jumlah = Penjumlahan(30,40)
print(jumlah)

print()
# Hampir sama dengan method __str__(), method __repr__() juga akan
# mengembalikan sebuah string. Bedanya, method __repr__() hanya
# akan dipanggil ketika kita memanggil default function repr() pada python.
class Kalimat:
    def __str__(self):
        return "output dari str"
    def __repr__(self):
        return "output dari repr"

a = Kalimat()
print(a)
print(str(a))
print(repr(a)) # tidak akan ada output jika tidak dipanggil

print()
#Di antara magic method yang cukup umum digunakan adalah: method __len__().
# Ia adalah sebuah method yang akan dipanggil ketika sebuah objek
# atau instan dijadikan parameter untuk fungsi bawaan python len().
class Siswa:
    def __init__(self):
        self.__list_siswa = ["contoh","nama","siswa"]
    def __len__(self):
        return len(self.__list_siswa)
    def __getitem__(self,index):
        return self.__list_siswa[index]

siswa = Siswa()
print(len(siswa))

print()
# Masih satu paket dengan fungsi __len__(). Fungsi __getitem__() adalah sebuah
# magic method yang akan dipanggil ketika sebuah instan dari kelas yang kita buat
# dipanggil menggunakan indeks yang diapit dengan kurung siku [].
#
# Mari kita lanjutkan kode program sebelumnya untuk kelas Siswa,
# kemudian kita akses instan dari kelas
# tersebut dengan kurung siku seperti berikut:
print(siswa[1])
