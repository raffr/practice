# static method, method bersifat satatic
# yang dapat dipanggil tanpa instance

class Contoh:
    @staticmethod
    def kali(angka1,angka2):
        print(angka1*angka2)

Contoh.kali(10,20)

print()

class Siswa:
    def __init__(self,nama):
        self.nama = nama
    @staticmethod
    def kapital(a):
        return a.upper()
    def output(self):
        print(self.nama)
        print(Siswa.kapital(self.nama))

andi = Siswa("andi")
andi.output()
