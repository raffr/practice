import paramiko
from getpass import getpass

class SshClient:
    def __init__(self,username, password, host):
        self.username = username
        self.password = password
        self.host = host
    def ssh_connect(self):
        self.client = paramiko.client.SSHClient()
        #self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.client.load_system_host_keys()
        self.client.connect(
            hostname = self.host,
            username = self.username,
            password = self.password
        )
    def ssh_exec_command(self,*command):
        for i in command:
            stdin, stdout, stderr = self.client.exec_command(i)
            print(f"\n {'='*10} execute '{i}' {'='*10} \n")
            print(stdout.read().decode())
        self.close()
    def close(self):
        self.client.close()


username = (input("username:") or "vm1")
hostname = (input("hostname:") or "192.168.122.129")
password = getpass("password")

server1 = SshClient(username,password,hostname)
server1.ssh_connect()
server1.ssh_exec_command("free -m","lsblk","echo hello world", "ip addr")
