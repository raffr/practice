# Overriding mengubah function / mereplace function induk dengan
# function baru
# Tambahkan Induk.function() jika ingin ada warisan dari induk function

class Hewan:
    def berjalan(self,kecepatan):
        print("berjalan dengan ",kecepatan)

class Predator(Hewan):
    def berjalan(self,kecepatan,mangsa):
        Hewan.berjalan(self,kecepatan)
        print("memangsa",mangsa )

siput = Hewan()
siput.berjalan("lambat")
singa = Predator()
singa.berjalan("cepat","kelinci")

print()

class Hewan_kedua:
    def __init__(self,nama,mangsa):
        self.nama = nama
        self.mangsa = mangsa
    def berjalan(self,kecepatan):
        return "berjalan dengan "+kecepatan
    def output(self,kecepatan):
        print("nama hewan:",self.nama)
        print("mangsa:",self.mangsa)
        print(self.berjalan(kecepatan))

class Hewan_kedua_Unggas(Hewan_kedua):
    def berjalan(self,kecepatan):
        return "terbang dengan "+ kecepatan

harimau = Hewan_kedua("Harimau","kelinci")
harimau.output(kecepatan="100km/hr")
print()
elang = Hewan_kedua_Unggas("elang","kelinci")
elang.output(kecepatan="100km/hr")
