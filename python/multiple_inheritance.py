class Hewan:
    def __init__(self,nama,kaki):
        self.nama = nama
        self.jumlah_kaki = kaki

class Habitat:
    def __init__(self,habitat):
        self.habitat = habitat

class Hewan_predator(Hewan,Habitat):
    def __init__(self,nama,kaki,mangsa,habitat):
        Hewan.__init__(self,nama,kaki)
        Habitat.__init__(self,habitat)
        self.mangsa = mangsa
    def output(self):
        kalimat = f"""
nama:{self.nama}
jumlah kaki:{self.jumlah_kaki}
habitat: {self.habitat}
mansa: {self.mangsa}
        """
        return kalimat

harimau = Hewan_predator("Harimau",4,"kelinci","hutan")
print(harimau.output())
