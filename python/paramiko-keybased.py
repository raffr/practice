import paramiko
import getpass
import os

class Ssh_Client:
    def __init__(self,username,host,keyfile):
        self.user = username
        self.host = host
        # Specify key
        self.key = paramiko.Ed25519Key.from_private_key_file(f"/home/user/.ssh/{keyfile}")
        # Connect to server
        self.client = paramiko.client.SSHClient()
        self.client.load_system_host_keys()
        self.client.connect(
            hostname = self.host,
            username = self.user,
            pkey = self.key
        )
    def exec_command(self, *command):
        for i in command:
            stdin, stdout, stderr = self.client.exec_command(i)
            print(f"{'='*10} execute '{i}'{'='*10}")
            print(stdout.read().decode())
    def close(self):
        self.client.close()


user_client = os.environ["USER"]

user = (input("username (default vm1): ") or "vm1")
hostname = (input("hostname (default 192.168.122.129): ") or "192.168.122.129")
key_location = input(f"key location (/home/{user_client}/.ssh/): ")


server1 = Ssh_Client(user,hostname,key_location)
server1.exec_command("uname -a", "free -m", "echo hello")
server1.close()