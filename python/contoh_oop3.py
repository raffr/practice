class siswa:
    def __init__(self,nama,umur,asal):
        self.nama = nama
        self.umur = umur
        self.asal = asal

    def perkenalan(self, pemilik_kucing):
        print("nama saya\t:",self.nama)
        print("umur saya\t:",self.umur)
        print("asal\t\t:",self.asal)
        print("kucing saya bernama :",pemilik_kucing.nama,)
        print("kucing saya berwarna:",pemilik_kucing.warna)

class kucing:
    def __init__(self,nama,warna):
        self.nama = nama
        self.warna = warna

print("")

kucing_buditabudi = kucing("oren","oranye")
buditabudi = siswa("budi tabudi",1234,"pluto")
buditabudi.perkenalan(kucing_buditabudi)

print("")
