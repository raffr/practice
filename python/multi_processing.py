import multiprocessing as mp
import time

"""

The threading module uses threads, the multiprocessing module uses processes. The difference is that threads run in the same memory space, while processes have separate memory. This makes it a bit harder to share objects between processes with multiprocessing. Since threads use the same memory, precautions have to be taken or two threads will write to the same memory at the same time. This is what the global interpreter lock is for.

"""


def calculationA(y):
    result = y**200000
    return result

def calculationB(y):
    result = y**200000
    return result

if __name__ == '__main__':

    start = time.time()
    # target is the function, args is the parameter of function
    p1 = mp.Process(target=calculationA, args=(2500,))
    p2 = mp.Process(target=calculationB, args=(2500,))
    end = time.time()

    print(end-start)

    start = time.time()
    print(calculationA(2500))
    print(calculationB(2500))
    end = time.time()

    print(end-start)
