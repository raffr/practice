import threading
import time

def func(limit):
    for i in range(limit):
        result = 20**25000
        #list_result.append(result)

def sec_func(limit):
    for i in range(limit):
        result = 20**30000
        #list_result.append(result)

if __name__ == "__main__":
#    list_result = []
    t1 = threading.Thread(target=func, name="t1", args=(10000,))
    t2 = threading.Thread(target=sec_func, name="t2", args=(10000,))

    start = time.time()
    t1.start()
    t2.start()
    t1.join()
    t2.join()
    end = time.time()

    print(end-start)

#start = time.time()
#func(10000)
#sec_func(10000)
#end = time.time()
#print(end-start)
