class Orang:
    def __init__(self,name,asal):
        self.name = name
        self.asal = asal
        print("ini merupakan Orang.__init__")
    def perkenalan(self):
        print(f"nama: {self.name}\nasal: {self.asal}")

class Pelajar(Orang):
    def __init__(self,name,asal):
        super().__init__(name,asal)
        self.name = name
        self.asal = asal
        print("saya adalah pelajar")

class Pekerja(Orang):
    def __init__(self,nama,asal):
        Orang.__init__(self,nama,asal)
        print("saya adalah pekerja")

andi = Pelajar("andi","pluto")
andi.perkenalan()
budi = Pekerja("Budi","mars")
budi.perkenalan()
