from django.shortcuts import render

# Create your views here.

def app(request):
    return render(request, "app/index.html")
def recent(request):
    return render(request, "app/recent.html")