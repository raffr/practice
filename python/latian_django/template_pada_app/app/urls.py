from django.urls import path
from . import views

urlpatterns = [
    path('',views.app),
    path('recent/',views.recent)
]