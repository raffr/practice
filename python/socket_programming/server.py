import socket

# get ip manually
# host = "192.168.1.70"

# get ip automatically in windows, in linux return localhost ip address
# host = socket.gethostbyname(socket.gethostname())



print(host)

port = 9090

# Accepting connection and for hosting, not a connection to client
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #TCP CONNECTION

server.bind((host, port))

server.listen(5)

while True:
    # communication_socket is a return for individual connection for every client,
    # and address is return an address from a client
    communication_socket, address = server.accept()
    print(f"connected to {address}")
    # receive a massage from client with 1024 byte, and decode it with utf-8
    massage_receive = communication_socket.recv(1024).decode("utf-8")
    print(f"massage from client is: {massage_receive}")
    # send a massage to client, and encode it with utf-8
    massage_send = communication_socket.send("your massage is received!".encode("utf-8"))
    # close connection between server and client
    communication_socket.close()
    print(f"connection with {address} is closed")

# close server
server.close()
