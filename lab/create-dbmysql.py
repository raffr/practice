import mysql.connector

class Database:
    def __init__(self):
        self.user = input("masukkan user:")
        self.password = input("masukkan password:")
        self.address = input("masukkan ip address:")
    def connect_mysql(self):
        self.db = mysql.connector.connect(
            user=self.user,
            passwd=self.password,
            host=self.address
        )
        self.cursor = self.db.cursor()
        if self.db.is_connected() == True:
            print("===\ndatabase is connected\n===")
        else:
            print("error")
    def create_database(self):
        self.dbinput = input("masukkan database yang akan dibuat:")
        create=f"create database {self.dbinput}"
        self.db.cursor().execute(create)
    def show_database(self):
        list_db = ("show databases")
        self.cursor.execute(list_db)
        for list_db in self.cursor:
            print(list_db)
    def drop_database(self):
        self.dbinput = input("masukkan database yang akand di drop:")
        self.cursor.execute(f"drop database {self.dbinput}")
        print(f"database {self.dbinput} dropped")

user = Database()
user.connect_mysql()

print("""
Pilih:
1. create database
2. show database
3. drop database
""")
pilih = int(input("masukkan pilihan: "))
if pilih == 1:
    user.create_database()
elif pilih == 2:
    user.show_database()
elif pilih == 3:
    user.drop_database()
else:
    print("input salah")