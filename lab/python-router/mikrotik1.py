import routeros_api
import json

# ip address mikrotik
host = '192.168.100.150'
connection = routeros_api.RouterOsApiPool(host=host,username='admin',password="0909",plaintext_login=True)
api=connection.get_api()

lsip = api.get_resource('ip/address/')
get_lsip = lsip.get()

print(json.dumps(get_lsip, indent=2))

connection.disconnect()